Shader "Unlit/WeaModelDetailsShader"
{
    Properties
    {
        _Threshold("Threshold", Range(0.0, 1.0)) = 0.1
        _Color("Color", Color) = (0, 0, 0)
        _SampleMap("Sample Map", 2D) = "black"
    }

    SubShader
    {
        Tags { "RenderType" = "Opaque" "RenderPipeline" = "UniversalRenderPipeline" }

        Pass
        {
            HLSLPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"            

            struct Attributes
            {
                float4 positionOS   : POSITION;
                float2 uv           : TEXCOORD0;
            };

            struct Varyings
            {
                float4 positionHCS  : SV_POSITION;
                float2 uv           : TEXCOORD0;
                float2 normalizedScreenSpace : TEXCOORD1;
            };

            TEXTURE2D(_SampleMap);
            SAMPLER(sampler_SampleMap);

            CBUFFER_START(UnityPerMaterial)
                float _Threshold;
                half4 _Color;
                float4 _SampleMap_ST;
                
            CBUFFER_END

            Varyings vert(Attributes IN)
            {
                Varyings OUT;
                OUT.positionHCS = TransformObjectToHClip(IN.positionOS.xyz);
                return OUT;
            }

            half4 frag(Varyings IN) : SV_Target
            {
                float2 normalizedScreenSpace = GetNormalizedScreenSpaceUV(IN.positionHCS);
                half4 sampleColor = SAMPLE_TEXTURE2D(_SampleMap, sampler_SampleMap, normalizedScreenSpace);
                //discard if blue value is higher than the threshold
                if (sampleColor.b < _Threshold) discard;
                //otherwise, sample the main texture as usual
                half4 color = _Color;
                return color;
            }

            ENDHLSL
        }
    }
}