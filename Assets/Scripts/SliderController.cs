using UnityEngine;
using UnityEngine.EventSystems;

public class SliderController : MonoBehaviour, IPointerUpHandler
{
    [SerializeField] private ModelManipulator manipulator;
    public void OnPointerUp(PointerEventData eventData)
    {
        manipulator.OnDragStop();
    }
}
