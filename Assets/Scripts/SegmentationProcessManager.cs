using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Xml;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class SegmentationProcessManager : MonoBehaviour
{
    public enum SegState
    {
        SkyAreaSelection,
        SkyConfirmAreaSelection,
        SkyThresholdAdjustment,
        CloudAreaSelection,
        CloudConfirmAreaSelection,
        CloudThresholdAdjustment
    }

    private enum CalibMode
    {
        Sky,
        Clouds
    }

    [SerializeField] Image snapshotImage;
    [SerializeField] Image overlayImage;

    [SerializeField] private float circleScaleSpeed;
    [SerializeField] Slider slider;

    [SerializeField] GameObject selectionCircle;
    [SerializeField] GameObject resultsUi;
    [SerializeField] GameObject centerButton;
    [SerializeField] GameObject cancelButton;
    [SerializeField] GameObject mainUi;

    [SerializeField] GameObject infoTextGameObject;
    [SerializeField] Sprite btnSpriteAdd;
    [SerializeField] Sprite btnSpriteSnapshot;
    [SerializeField] Sprite btnSpriteConfirm;
    [SerializeField] Sprite btnSpriteConfirmInactive;
    [SerializeField] Text progressText;
    [SerializeField] Text infoTextComponent;

    [SerializeField] Text WOLKENANTEIL_GERING;
    [SerializeField] Text WOLKENANTEIL_MITTEL;
    [SerializeField] Text WOLKENANTEIL_HOCH;
    [SerializeField] Text ZEIT_MORGENS;
    [SerializeField] Text ZEIT_MITTAGS;
    [SerializeField] Text ZEIT_ABENDS;
    [SerializeField] Text WOLKEN_HELL;
    [SerializeField] Text WOLKEN_GEMISCHT;
    [SerializeField] Text WOLKEN_DUNKEL;
    [SerializeField] Text STRASSE;
    [SerializeField] Text GEBAEUDE;
    [SerializeField] Text BRUECKE;
    [SerializeField] Text WASSER;
    [SerializeField] Text FELD;
    [SerializeField] Text GESAMT;

    private List<Result> resultsList = new List<Result>();
    [SerializeField] private PrecisionCalculator precisionCalculator;
    [SerializeField] private PictureClassificationDatabase classificationDatabase;

    Image centerButtonImage;
    SegState currState = SegState.SkyAreaSelection;
    Texture2D snapshotTex;
    Texture2D overlayTex;
    Color overlaySkyColor = new Color(0, 0, 1, 0.5f);
    Color overlayGroundColor = new Color(1, 0, 0, 0.5f);
    float avgBlueSky;
    float avgBlueCloud;
    float thresholdSky;
    float thresholdCloud;
    private int progress = 1;
    bool touchRegistered = false;
    Vector2 circlePosition;
    float touchDuration;

    private void Awake()
    {
        slider.onValueChanged.AddListener(delegate { OnSliderValueChanged(); });
        centerButtonImage = centerButton.GetComponent<Image>();
        progressText.text = "Progress: 0 / 103 Images";
    }

    private void Update()
    {
        if (touchRegistered && (currState == SegState.SkyAreaSelection || currState == SegState.CloudAreaSelection))
        {
            touchDuration += Time.deltaTime;
            AdjustSelectionCircle();
        }

        if (Input.GetKeyDown(KeyCode.Escape)) RegressSegState();
    }

    public void ProgressSegState()
    {
        switch (currState)
        {
            case SegState.SkyAreaSelection:
                EnterSkyConfirmAreaSelectionMode();
                break;
            case SegState.SkyConfirmAreaSelection:
                EnterSkyThresholdAdjustmentMode();
                break;
            case SegState.SkyThresholdAdjustment:
                EnterCloudAreaSelectionMode();
                break;
            case SegState.CloudAreaSelection:
                EnterCloudConfirmAreaSelectionMode();
                break;
            case SegState.CloudConfirmAreaSelection:
                EnterCloudThresholdAdjustmentMode();
                break;
            case SegState.CloudThresholdAdjustment:
                OnConfirmSegmentation();
                break;
        }
    }

    public void RegressSegState()
    {
        switch (currState)
        {
            case SegState.SkyAreaSelection:
                break;
            case SegState.SkyConfirmAreaSelection:
                EnterSkyAreaSelectionMode();
                break;
            case SegState.SkyThresholdAdjustment:
                EnterSkyConfirmAreaSelectionMode();
                break;
            case SegState.CloudAreaSelection:
                break;
            case SegState.CloudConfirmAreaSelection:
                EnterCloudAreaSelectionMode();
                break;
            case SegState.CloudThresholdAdjustment:
                EnterCloudConfirmAreaSelectionMode();
                break;
        }
    }

    public SegState GetSegState()
    {
        return currState;
    }

    public void EnterSkyAreaSelectionMode()
    {
        currState = SegState.SkyAreaSelection;

        centerButtonImage.sprite = btnSpriteConfirmInactive;

        cancelButton.SetActive(false);

        snapshotImage.gameObject.SetActive(true);

        overlayImage.gameObject.SetActive(false);

        slider.gameObject.SetActive(false);

        selectionCircle.SetActive(false);

        infoTextGameObject.SetActive(true);
        infoTextComponent.text = "Tap and hold the sky!";
    }

    void EnterSkyConfirmAreaSelectionMode()
    {
        currState = SegState.SkyConfirmAreaSelection;

        centerButtonImage.sprite = btnSpriteConfirm;

        cancelButton.SetActive(true);

        snapshotImage.gameObject.SetActive(true);

        overlayImage.gameObject.SetActive(false);

        slider.gameObject.SetActive(false);

        selectionCircle.SetActive(true);

        infoTextGameObject.SetActive(true);
        infoTextComponent.text = "Is the sky selection okay?";
    }

    void EnterSkyThresholdAdjustmentMode()
    {
        currState = SegState.SkyThresholdAdjustment;

        centerButtonImage.sprite = btnSpriteConfirm;

        cancelButton.SetActive(true);

        snapshotImage.gameObject.SetActive(true);

        overlayImage.gameObject.SetActive(true);

        slider.gameObject.SetActive(true);
        OnSliderValueChanged();

        avgBlueSky = CalculateAverageSelectedColor();
        GenerateOverlaySprite(CalibMode.Sky);

        selectionCircle.SetActive(false);

        infoTextGameObject.SetActive(true);
        infoTextComponent.text = "Adjust the slider until only the sky is tinted blue!";
    }

    void EnterCloudAreaSelectionMode()
    {
        currState = SegState.CloudAreaSelection;

        centerButtonImage.sprite = btnSpriteConfirmInactive;

        cancelButton.SetActive(false);

        snapshotImage.gameObject.SetActive(true);

        overlayImage.gameObject.SetActive(false);

        slider.gameObject.SetActive(false);

        selectionCircle.SetActive(false);

        infoTextGameObject.SetActive(true);
        infoTextComponent.text = "Tap and hold the clouds!";
    }

    void EnterCloudConfirmAreaSelectionMode()
    {
        currState = SegState.CloudConfirmAreaSelection;

        centerButtonImage.sprite = btnSpriteConfirm;

        cancelButton.SetActive(true);

        snapshotImage.gameObject.SetActive(true);

        overlayImage.gameObject.SetActive(false);

        slider.gameObject.SetActive(false);

        selectionCircle.SetActive(true);

        infoTextGameObject.SetActive(true);
        infoTextComponent.text = "Is the cloud selection okay?";
    }

    void EnterCloudThresholdAdjustmentMode()
    {
        currState = SegState.CloudThresholdAdjustment;

        centerButtonImage.sprite = btnSpriteConfirm;

        cancelButton.SetActive(true);

        snapshotImage.gameObject.SetActive(true);

        overlayImage.gameObject.SetActive(true);

        slider.gameObject.SetActive(true);
        OnSliderValueChanged();

        avgBlueCloud = CalculateAverageSelectedColor();
        GenerateOverlaySprite(CalibMode.Clouds);

        selectionCircle.SetActive(false);

        infoTextGameObject.SetActive(true);
        infoTextComponent.text = "Adjust the slider until all clouds and the sky are tinted blue!";
    }

    public void OnConfirmSegmentation()
    {
        //current picture being processed
        string filename = String.Format("{0:000}", progress);

        //create result class with filename and int[] that contains classification result
        Result result = new Result(filename, FinalClassifyTexture());

        //add it to the list of all results
        resultsList.Add(result);

        //do precision math, convert progress nr. to list position
        resultsList[resultsList.Count - 1].precision = precisionCalculator.CalculatePrecision(resultsList[resultsList.Count - 1]);

        //persist results, just in case
        string path = Application.persistentDataPath + "/Results/" + filename + ".json";
        Directory.CreateDirectory(Path.GetDirectoryName(path));
        File.WriteAllText(path, null);
        string resultJson = JsonUtility.ToJson(result);
        File.WriteAllText(path, resultJson);

        //update the infotext
        progressText.text = "Progress: " + progress + " / 103 Images";

        //load next pic
        progress++;
        filename = String.Format("{0:000}", progress);
        string filepath = "Images/ImageRepository/" + filename;
        
        Sprite nextSnapShot = Resources.Load<Sprite>(filepath);
        Resources.UnloadAsset(snapshotImage.sprite);

        //if there are no more pics to process, end the evaluation
        if (nextSnapShot == null)
        {
            EndEvaluation();
             
            return;
        }
        //otherwise, load the sprite and goto the start of the process
        snapshotImage.sprite = nextSnapShot;

        Resources.UnloadUnusedAssets();
        GC.Collect();

        EnterSkyAreaSelectionMode();
    }

    private void EndEvaluation()
    {
        mainUi.SetActive(false);

        //do statistics math
        float[] statistics = classificationDatabase.CalculateStatistics(resultsList);

        ResultsOverview resultsOverview = new ResultsOverview();
        resultsOverview.resultsArray = statistics;

        //TODO add the correct text fields and assign the values depending on the position of the tag in the enum!
        WOLKENANTEIL_GERING.text = statistics[0].ToString("P", CultureInfo.InvariantCulture);
        WOLKENANTEIL_MITTEL.text = statistics[1].ToString("P", CultureInfo.InvariantCulture);
        WOLKENANTEIL_HOCH.text = statistics[2].ToString("P", CultureInfo.InvariantCulture);
        ZEIT_MORGENS.text = statistics[3].ToString("P", CultureInfo.InvariantCulture);
        ZEIT_MITTAGS.text = statistics[4].ToString("P", CultureInfo.InvariantCulture);
        ZEIT_ABENDS.text = statistics[5].ToString("P", CultureInfo.InvariantCulture);
        WOLKEN_HELL.text = statistics[6].ToString("P", CultureInfo.InvariantCulture);
        WOLKEN_GEMISCHT.text = statistics[7].ToString("P", CultureInfo.InvariantCulture);
        WOLKEN_DUNKEL.text = statistics[8].ToString("P", CultureInfo.InvariantCulture);
        STRASSE.text = statistics[9].ToString("P", CultureInfo.InvariantCulture);
        GEBAEUDE.text = statistics[10].ToString("P", CultureInfo.InvariantCulture);
        BRUECKE.text = statistics[11].ToString("P", CultureInfo.InvariantCulture);
        WASSER.text = statistics[12].ToString("P", CultureInfo.InvariantCulture);
        FELD.text = statistics[13].ToString("P", CultureInfo.InvariantCulture);
        GESAMT.text = statistics[14].ToString("P", CultureInfo.InvariantCulture);

        //write results json
        string path = Application.persistentDataPath + "/Results/resultsOverview.json";
        Directory.CreateDirectory(Path.GetDirectoryName(path));
        File.WriteAllText(path, null);
        string resultsOverviewJson = JsonUtility.ToJson(resultsOverview);
        File.WriteAllText(path, resultsOverviewJson);

        //present results
        resultsUi.SetActive(true);
    }

    private void CheckEvaluationResults()
    {
        string filename = String.Format("{0:000}", progress);
        string path = Application.persistentDataPath + "/Results/" + filename + ".json";
        String jsonString = File.ReadAllText(path);
        Result readResult = JsonUtility.FromJson<Result>(jsonString);

        overlayTex = new Texture2D(snapshotImage.mainTexture.width, snapshotImage.mainTexture.height, TextureFormat.RGBA32, false, true);
        overlayTex.filterMode = FilterMode.Point;
        Color[] colors = new Color[readResult.ResultBools.Length];
        for (int i = 0; i < colors.Length; i++)
        {
            if (readResult.ResultBools[i] == 1) colors[i] = Color.black;
            else colors[i] = Color.white;
        }
        overlayTex.SetPixels(colors);

        overlayTex.Apply();

        overlayImage.enabled = true;

        //snapshotImage.sprite = GenerateSprite(overlayTex);

        EnterSkyAreaSelectionMode();
    }



    void OnSliderValueChanged()
    {
        if (currState == SegState.SkyThresholdAdjustment)
        {
            thresholdSky = slider.value;
            GenerateOverlaySprite(CalibMode.Sky);
        }

        if (currState == SegState.CloudThresholdAdjustment)
        {
            thresholdCloud = slider.value;
            GenerateOverlaySprite(CalibMode.Clouds);
        }
    }

    void AdjustSelectionCircle()
    {
        selectionCircle.transform.position = circlePosition;
        selectionCircle.transform.localScale = Vector3.one * touchDuration * circleScaleSpeed;
    }

    float CalculateAverageSelectedColor()
    {
        //screenspace position of circle
        int xCoord = (int)circlePosition.x;
        int yCoord = (int)circlePosition.y;

        //screenspace position of first pixel in texture
        int xOffset =  (Screen.width - snapshotImage.sprite.texture.width) / 2;
        int yOffset =  (Screen.height - snapshotImage.sprite.texture.height) / 2;

        //amount of pixels in radius
        float circleRadius = (selectionCircle.transform.localScale.x * 256f) * 0.5f;
        int pixelRadius = (int)circleRadius;

        int textureWidth = snapshotImage.sprite.texture.width;
        int textureHeight = snapshotImage.sprite.texture.height;
        int addedPixels = 0;
        Color[] selectedColors = snapshotImage.sprite.texture.GetPixels();

        float avgBlue = 0;

        for (int i = 0; i < textureHeight; i++)
        {
            for (int j = 0; j < textureWidth; j++)
            {
                float xDist = xCoord - (j + xOffset);
                float yDist = yCoord - (i + yOffset);

                float distance = Mathf.Sqrt(Mathf.Pow(xDist, 2) + Mathf.Pow(yDist, 2));

                if (distance < circleRadius)
                {
                    avgBlue += selectedColors[j + i * textureWidth].b;
                    addedPixels++;
                }
            }
        }
        avgBlue = avgBlue / (float)addedPixels;
        return avgBlue;
    }

    public void OnFingerDown(PointerEventData eventData)
    {
        selectionCircle.SetActive(true);
        touchRegistered = true;
        circlePosition = eventData.position;
    }

    public void OnFingerUp(PointerEventData eventData)
    {
        touchRegistered = false;
        touchDuration = 0;
        if (currState == SegState.SkyAreaSelection || currState == SegState.CloudAreaSelection) ProgressSegState();
    }

    Sprite GenerateSprite(Texture2D texture)
    {
        Sprite sprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), new Vector2(0.5f, 0.5f), 1f, 0, SpriteMeshType.FullRect);
        sprite.name = "snapshotSprite";
        return sprite;
    }

    void GenerateOverlaySprite(CalibMode mode)
    {
        DestroyImmediate(overlayImage.sprite.texture);
        DestroyImmediate(overlayImage.sprite);

        overlayTex = new Texture2D(snapshotImage.mainTexture.width, snapshotImage.mainTexture.height, TextureFormat.RGBA32, false);

        overlayTex.SetPixels(ClassifyTexture(mode));
        
        overlayTex.Apply();

        overlayImage.enabled = true;
        
        overlayImage.sprite = GenerateSprite(overlayTex);
    }

    int[] FinalClassifyTexture()
    {
        Color[] colorArr = ((Texture2D)snapshotImage.mainTexture).GetPixels();
        int length = colorArr.Length;
        int[] result = new int[length];
        int[] samplePositions = new int[4];
        int width = snapshotImage.mainTexture.width;
        int height = snapshotImage.mainTexture.height;
        for (int i = 0; i < colorArr.Length; i++)
        {
            //sampled pixels are diagonal neighbors
            //if current pixel is on the side of the pixel, the sampled pixels need to be offset differently

            // |s p p
            // |p x p
            // |
            // |
            int leftOffset = (i % width == 0) ? 0 : -1;
            int rightOffset = (i % width == (width - 1)) ? 0 : 1;
            int bottomOffset = (i < width) ? 0 : -1;
            int topOffset = (i >= (length - width)) ? 0 : 1;
            samplePositions[0] = i + (topOffset * width) + leftOffset;
            samplePositions[1] = i + (topOffset * width) + rightOffset;
            samplePositions[2] = i + (bottomOffset * width) + leftOffset;
            samplePositions[3] = i + (bottomOffset * width) + rightOffset;
            if 
            (
                (ClassifyPixel(colorArr[samplePositions[0]], CalibMode.Sky) &
                 ClassifyPixel(colorArr[samplePositions[1]], CalibMode.Sky) &
                 ClassifyPixel(colorArr[samplePositions[2]], CalibMode.Sky) &
                 ClassifyPixel(colorArr[samplePositions[3]], CalibMode.Sky)) 
                
                ||
            
                (ClassifyPixel(colorArr[samplePositions[0]], CalibMode.Clouds) &
                 ClassifyPixel(colorArr[samplePositions[1]], CalibMode.Clouds) &
                 ClassifyPixel(colorArr[samplePositions[2]], CalibMode.Clouds) &
                 ClassifyPixel(colorArr[samplePositions[3]], CalibMode.Clouds))

            ) result[i] = 1;

            else result[i] = 0;
        }
        return result;
    }

    Color[] ClassifyTexture(CalibMode mode)
    {
        Color[] colorArr = ((Texture2D)snapshotImage.mainTexture).GetPixels();
        int length = colorArr.Length;
        Color[] result = new Color[length];
        int[] samplePositions = new int[4];
        int width = snapshotImage.mainTexture.width;
        int height = snapshotImage.mainTexture.height;
        for (int i = 0; i < colorArr.Length; i++)
        {
            int leftOffset = (i % width == 0) ? 0 : -1;
            int rightOffset = (i % width == (width - 1)) ? 0 : 1;
            int bottomOffset = (i < width) ? 0 : -1;
            int topOffset = (i >= (length - width)) ? 0 : 1;
            samplePositions[0] = i + (topOffset * width) + leftOffset;
            samplePositions[1] = i + (topOffset * width) + rightOffset;
            samplePositions[2] = i + (bottomOffset * width) + leftOffset;
            samplePositions[3] = i + (bottomOffset * width) + rightOffset;
            if (ClassifyPixel(colorArr[samplePositions[0]], mode) &&
                ClassifyPixel(colorArr[samplePositions[1]], mode) &&
                ClassifyPixel(colorArr[samplePositions[2]], mode) &&
                ClassifyPixel(colorArr[samplePositions[3]], mode)) result[i] = overlaySkyColor;
            else result[i] = overlayGroundColor;
        }
        return result;
    }

    bool ClassifyPixel(Color color, CalibMode mode)
    {
        bool classificationResult = false;

        switch (mode)
        {
            case CalibMode.Sky:
                if (Mathf.Abs(color.b - avgBlueSky) < thresholdSky) classificationResult = true;
                break;
            case CalibMode.Clouds:
                if (Mathf.Abs(color.b - avgBlueCloud) < thresholdCloud || Mathf.Abs(color.b - avgBlueSky) < thresholdSky) classificationResult = true;
                break;
        }

        return classificationResult;
    }
}
