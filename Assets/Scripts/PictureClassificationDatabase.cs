using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PictureClassificationDatabase : MonoBehaviour
{
    public enum Tags
    {
        WOLKENANTEIL_GERING,
        WOLKENANTEIL_MITTEL,
        WOLKENANTEIL_HOCH,

        ZEIT_MORGENS,
        ZEIT_MITTAGS,
        ZEIT_ABENDS,

        WOLKEN_HELL,
        WOLKEN_GEMISCHT,
        WOLKEN_DUNKEL,

        STRASSE,
        GEBAEUDE,
        BRUECKE,
        WASSER,
        FELD, 
        GESAMT
    }

    private List<Picture> picDatabase;

    void Start()
    {
        picDatabase = new List<Picture>();
        picDatabase.Add(CreatePicture("001", Tags.GESAMT, Tags.ZEIT_MITTAGS, Tags.WOLKENANTEIL_MITTEL, Tags.WOLKEN_HELL, Tags.GEBAEUDE));
        picDatabase.Add(CreatePicture("002", Tags.GESAMT, Tags.ZEIT_MITTAGS, Tags.WOLKENANTEIL_HOCH, Tags.WOLKEN_GEMISCHT, Tags.GEBAEUDE));
        picDatabase.Add(CreatePicture("003", Tags.GESAMT, Tags.ZEIT_MITTAGS, Tags.WOLKENANTEIL_HOCH, Tags.WOLKEN_DUNKEL, Tags.GEBAEUDE, Tags.WASSER));
        picDatabase.Add(CreatePicture("004", Tags.GESAMT, Tags.ZEIT_MITTAGS, Tags.WOLKENANTEIL_HOCH, Tags.WOLKEN_GEMISCHT, Tags.STRASSE));
        picDatabase.Add(CreatePicture("005", Tags.GESAMT, Tags.ZEIT_MITTAGS, Tags.WOLKENANTEIL_HOCH, Tags.WOLKEN_GEMISCHT, Tags.STRASSE, Tags.GEBAEUDE));
        picDatabase.Add(CreatePicture("006", Tags.GESAMT, Tags.ZEIT_MITTAGS, Tags.WOLKENANTEIL_HOCH, Tags.WOLKEN_GEMISCHT, Tags.GEBAEUDE, Tags.STRASSE));
        picDatabase.Add(CreatePicture("007", Tags.GESAMT, Tags.ZEIT_MITTAGS, Tags.WOLKENANTEIL_HOCH, Tags.WOLKEN_GEMISCHT, Tags.STRASSE, Tags.GEBAEUDE, Tags.BRUECKE));
        picDatabase.Add(CreatePicture("008", Tags.GESAMT, Tags.ZEIT_MITTAGS, Tags.WOLKENANTEIL_MITTEL, Tags.WOLKEN_HELL, Tags.STRASSE));
        picDatabase.Add(CreatePicture("009", Tags.GESAMT, Tags.ZEIT_MITTAGS, Tags.WOLKENANTEIL_HOCH, Tags.WOLKEN_GEMISCHT));
        picDatabase.Add(CreatePicture("010", Tags.GESAMT, Tags.ZEIT_MITTAGS, Tags.WOLKENANTEIL_HOCH, Tags.WOLKEN_GEMISCHT, Tags.FELD));
        picDatabase.Add(CreatePicture("011", Tags.GESAMT, Tags.ZEIT_MITTAGS, Tags.WOLKENANTEIL_MITTEL, Tags.WOLKEN_GEMISCHT, Tags.STRASSE, Tags.FELD));
        picDatabase.Add(CreatePicture("012", Tags.GESAMT, Tags.ZEIT_MITTAGS, Tags.WOLKENANTEIL_HOCH, Tags.WOLKEN_DUNKEL, Tags.FELD));
        picDatabase.Add(CreatePicture("013", Tags.GESAMT, Tags.ZEIT_MITTAGS, Tags.WOLKENANTEIL_HOCH, Tags.WOLKEN_GEMISCHT, Tags.FELD));
        picDatabase.Add(CreatePicture("014", Tags.GESAMT, Tags.ZEIT_MITTAGS, Tags.WOLKENANTEIL_MITTEL, Tags.WOLKEN_HELL, Tags.FELD));
        picDatabase.Add(CreatePicture("015", Tags.GESAMT, Tags.ZEIT_MITTAGS, Tags.WOLKENANTEIL_MITTEL, Tags.WOLKEN_HELL, Tags.FELD));
        picDatabase.Add(CreatePicture("016", Tags.GESAMT, Tags.ZEIT_MITTAGS, Tags.WOLKENANTEIL_MITTEL, Tags.WOLKEN_GEMISCHT, Tags.STRASSE, Tags.FELD));
        picDatabase.Add(CreatePicture("017", Tags.GESAMT, Tags.ZEIT_MITTAGS, Tags.WOLKENANTEIL_HOCH, Tags.WOLKEN_DUNKEL));
        picDatabase.Add(CreatePicture("018", Tags.GESAMT, Tags.ZEIT_MITTAGS, Tags.WOLKENANTEIL_HOCH, Tags.WOLKEN_DUNKEL));
        picDatabase.Add(CreatePicture("019", Tags.GESAMT, Tags.ZEIT_MITTAGS, Tags.WOLKENANTEIL_HOCH, Tags.WOLKEN_DUNKEL, Tags.STRASSE, Tags.GEBAEUDE));
        picDatabase.Add(CreatePicture("020", Tags.GESAMT, Tags.ZEIT_MITTAGS, Tags.WOLKENANTEIL_MITTEL, Tags.WOLKEN_HELL));
        picDatabase.Add(CreatePicture("021", Tags.GESAMT, Tags.ZEIT_MITTAGS, Tags.WOLKENANTEIL_HOCH, Tags.WOLKEN_GEMISCHT, Tags.STRASSE, Tags.GEBAEUDE));
        picDatabase.Add(CreatePicture("022", Tags.GESAMT, Tags.ZEIT_MITTAGS, Tags.WOLKENANTEIL_HOCH, Tags.WOLKEN_GEMISCHT, Tags.STRASSE, Tags.GEBAEUDE));
        picDatabase.Add(CreatePicture("023", Tags.GESAMT, Tags.ZEIT_MITTAGS, Tags.WOLKENANTEIL_HOCH, Tags.WOLKEN_GEMISCHT, Tags.STRASSE));
        picDatabase.Add(CreatePicture("024", Tags.GESAMT, Tags.ZEIT_MITTAGS, Tags.WOLKENANTEIL_HOCH, Tags.WOLKEN_GEMISCHT, Tags.STRASSE));
        picDatabase.Add(CreatePicture("025", Tags.GESAMT, Tags.ZEIT_MITTAGS, Tags.WOLKENANTEIL_HOCH, Tags.WOLKEN_GEMISCHT, Tags.WASSER));
        picDatabase.Add(CreatePicture("026", Tags.GESAMT, Tags.ZEIT_MITTAGS, Tags.WOLKENANTEIL_HOCH, Tags.WOLKEN_GEMISCHT, Tags.WASSER));
        picDatabase.Add(CreatePicture("027", Tags.GESAMT, Tags.ZEIT_MITTAGS, Tags.WOLKENANTEIL_MITTEL, Tags.WOLKEN_GEMISCHT, Tags.BRUECKE, Tags.WASSER));
        picDatabase.Add(CreatePicture("028", Tags.GESAMT, Tags.ZEIT_MITTAGS, Tags.WOLKENANTEIL_HOCH, Tags.WOLKEN_GEMISCHT, Tags.BRUECKE, Tags.WASSER));
        picDatabase.Add(CreatePicture("029", Tags.GESAMT, Tags.ZEIT_MITTAGS, Tags.WOLKENANTEIL_HOCH, Tags.WOLKEN_DUNKEL, Tags.GEBAEUDE, Tags.FELD));
        picDatabase.Add(CreatePicture("030", Tags.GESAMT, Tags.ZEIT_MITTAGS, Tags.WOLKENANTEIL_HOCH, Tags.WOLKEN_GEMISCHT, Tags.GEBAEUDE));
        picDatabase.Add(CreatePicture("031", Tags.GESAMT, Tags.ZEIT_MITTAGS, Tags.WOLKENANTEIL_MITTEL, Tags.WOLKEN_GEMISCHT, Tags.STRASSE, Tags.GEBAEUDE));
        picDatabase.Add(CreatePicture("032", Tags.GESAMT, Tags.ZEIT_MITTAGS, Tags.WOLKENANTEIL_MITTEL, Tags.WOLKEN_DUNKEL, Tags.STRASSE));
        picDatabase.Add(CreatePicture("033", Tags.GESAMT, Tags.ZEIT_MITTAGS, Tags.WOLKENANTEIL_HOCH, Tags.WOLKEN_DUNKEL, Tags.WASSER, Tags.GEBAEUDE));
        picDatabase.Add(CreatePicture("034", Tags.GESAMT, Tags.ZEIT_MITTAGS, Tags.WOLKENANTEIL_HOCH, Tags.WOLKEN_GEMISCHT, Tags.WASSER, Tags.GEBAEUDE));
        picDatabase.Add(CreatePicture("035", Tags.GESAMT, Tags.ZEIT_MITTAGS, Tags.WOLKENANTEIL_MITTEL, Tags.WOLKEN_HELL, Tags.STRASSE));
        picDatabase.Add(CreatePicture("036", Tags.GESAMT, Tags.ZEIT_MITTAGS, Tags.WOLKENANTEIL_HOCH, Tags.WOLKEN_HELL, Tags.WASSER));
        picDatabase.Add(CreatePicture("037", Tags.GESAMT, Tags.ZEIT_MITTAGS, Tags.WOLKENANTEIL_HOCH, Tags.WOLKEN_HELL, Tags.STRASSE));
        picDatabase.Add(CreatePicture("038", Tags.GESAMT, Tags.ZEIT_MITTAGS, Tags.WOLKENANTEIL_HOCH, Tags.WOLKEN_HELL, Tags.STRASSE, Tags.GEBAEUDE));
        picDatabase.Add(CreatePicture("039", Tags.GESAMT, Tags.ZEIT_MITTAGS, Tags.WOLKENANTEIL_HOCH, Tags.WOLKEN_HELL, Tags.STRASSE, Tags.GEBAEUDE));
        picDatabase.Add(CreatePicture("040", Tags.GESAMT, Tags.ZEIT_MITTAGS, Tags.WOLKENANTEIL_HOCH, Tags.WOLKEN_GEMISCHT, Tags.WASSER));
        picDatabase.Add(CreatePicture("041", Tags.GESAMT, Tags.ZEIT_MITTAGS, Tags.WOLKENANTEIL_HOCH, Tags.WOLKEN_GEMISCHT, Tags.WASSER));
        picDatabase.Add(CreatePicture("042", Tags.GESAMT, Tags.ZEIT_MITTAGS, Tags.WOLKENANTEIL_MITTEL, Tags.WOLKEN_HELL, Tags.WASSER));
        picDatabase.Add(CreatePicture("043", Tags.GESAMT, Tags.ZEIT_MITTAGS, Tags.WOLKENANTEIL_HOCH, Tags.WOLKEN_GEMISCHT));
        picDatabase.Add(CreatePicture("044", Tags.GESAMT, Tags.ZEIT_MITTAGS, Tags.WOLKENANTEIL_MITTEL, Tags.WOLKEN_GEMISCHT, Tags.WASSER));
        picDatabase.Add(CreatePicture("045", Tags.GESAMT, Tags.ZEIT_MITTAGS, Tags.WOLKENANTEIL_MITTEL, Tags.WOLKEN_HELL, Tags.FELD));
        picDatabase.Add(CreatePicture("046", Tags.GESAMT, Tags.ZEIT_MITTAGS, Tags.WOLKENANTEIL_MITTEL, Tags.WOLKEN_HELL, Tags.WASSER));
        picDatabase.Add(CreatePicture("047", Tags.GESAMT, Tags.ZEIT_MITTAGS, Tags.WOLKENANTEIL_MITTEL, Tags.WOLKEN_GEMISCHT));
        picDatabase.Add(CreatePicture("048", Tags.GESAMT, Tags.ZEIT_MITTAGS, Tags.WOLKENANTEIL_MITTEL, Tags.WOLKEN_GEMISCHT, Tags.WASSER, Tags.BRUECKE));
        picDatabase.Add(CreatePicture("049", Tags.GESAMT, Tags.ZEIT_MITTAGS, Tags.WOLKENANTEIL_GERING, Tags.WOLKEN_HELL, Tags.STRASSE, Tags.GEBAEUDE));
        picDatabase.Add(CreatePicture("050", Tags.GESAMT, Tags.ZEIT_MITTAGS, Tags.WOLKENANTEIL_MITTEL, Tags.WOLKEN_GEMISCHT, Tags.WASSER));
        picDatabase.Add(CreatePicture("051", Tags.GESAMT, Tags.ZEIT_MITTAGS, Tags.WOLKENANTEIL_HOCH, Tags.WOLKEN_GEMISCHT, Tags.GEBAEUDE));
        picDatabase.Add(CreatePicture("052", Tags.GESAMT, Tags.ZEIT_MITTAGS, Tags.WOLKENANTEIL_HOCH, Tags.WOLKEN_GEMISCHT, Tags.GEBAEUDE, Tags.STRASSE));
        picDatabase.Add(CreatePicture("053", Tags.GESAMT, Tags.ZEIT_MITTAGS, Tags.WOLKENANTEIL_HOCH, Tags.WOLKEN_GEMISCHT, Tags.FELD));
        picDatabase.Add(CreatePicture("054", Tags.GESAMT, Tags.ZEIT_MITTAGS, Tags.WOLKENANTEIL_HOCH, Tags.WOLKEN_GEMISCHT, Tags.FELD));
        picDatabase.Add(CreatePicture("055", Tags.GESAMT, Tags.ZEIT_MITTAGS, Tags.WOLKENANTEIL_HOCH, Tags.WOLKEN_HELL, Tags.FELD));
        picDatabase.Add(CreatePicture("056", Tags.GESAMT, Tags.ZEIT_MITTAGS, Tags.WOLKENANTEIL_HOCH, Tags.WOLKEN_GEMISCHT, Tags.FELD)); 
        picDatabase.Add(CreatePicture("057", Tags.GESAMT, Tags.ZEIT_MITTAGS, Tags.WOLKENANTEIL_HOCH, Tags.WOLKEN_GEMISCHT, Tags.GEBAEUDE));
        picDatabase.Add(CreatePicture("058", Tags.GESAMT, Tags.ZEIT_MITTAGS, Tags.WOLKENANTEIL_GERING, Tags.WOLKEN_HELL, Tags.WASSER));
        picDatabase.Add(CreatePicture("059", Tags.GESAMT, Tags.ZEIT_MITTAGS, Tags.WOLKENANTEIL_MITTEL, Tags.WOLKEN_GEMISCHT, Tags.FELD));
        picDatabase.Add(CreatePicture("060", Tags.GESAMT, Tags.ZEIT_MITTAGS, Tags.WOLKENANTEIL_HOCH, Tags.WOLKEN_DUNKEL, Tags.FELD, Tags.STRASSE));
        picDatabase.Add(CreatePicture("061", Tags.GESAMT, Tags.ZEIT_MITTAGS, Tags.WOLKENANTEIL_HOCH, Tags.WOLKEN_HELL, Tags.WASSER, Tags.GEBAEUDE));
        picDatabase.Add(CreatePicture("062", Tags.GESAMT, Tags.ZEIT_MITTAGS, Tags.WOLKENANTEIL_HOCH, Tags.WOLKEN_HELL, Tags.STRASSE, Tags.WASSER));
        picDatabase.Add(CreatePicture("063", Tags.GESAMT, Tags.ZEIT_MITTAGS, Tags.WOLKENANTEIL_HOCH, Tags.WOLKEN_GEMISCHT, Tags.WASSER, Tags.STRASSE));
        picDatabase.Add(CreatePicture("064", Tags.GESAMT, Tags.ZEIT_MITTAGS, Tags.WOLKENANTEIL_MITTEL, Tags.WOLKEN_HELL, Tags.GEBAEUDE, Tags.WASSER));
        picDatabase.Add(CreatePicture("065", Tags.GESAMT, Tags.ZEIT_MITTAGS, Tags.WOLKENANTEIL_MITTEL, Tags.WOLKEN_HELL, Tags.WASSER, Tags.GEBAEUDE));
        picDatabase.Add(CreatePicture("066", Tags.GESAMT, Tags.ZEIT_MITTAGS, Tags.WOLKENANTEIL_MITTEL, Tags.WOLKEN_GEMISCHT, Tags.WASSER));
        picDatabase.Add(CreatePicture("067", Tags.GESAMT, Tags.ZEIT_MITTAGS, Tags.WOLKENANTEIL_MITTEL, Tags.WOLKEN_GEMISCHT, Tags.GEBAEUDE, Tags.WASSER));
        picDatabase.Add(CreatePicture("068", Tags.GESAMT, Tags.ZEIT_MITTAGS, Tags.WOLKENANTEIL_HOCH, Tags.WOLKEN_GEMISCHT, Tags.STRASSE));
        picDatabase.Add(CreatePicture("069", Tags.GESAMT, Tags.ZEIT_ABENDS, Tags.WOLKENANTEIL_GERING, Tags.WOLKEN_HELL, Tags.GEBAEUDE));
        picDatabase.Add(CreatePicture("070", Tags.GESAMT, Tags.ZEIT_ABENDS, Tags.WOLKENANTEIL_MITTEL, Tags.WOLKEN_GEMISCHT, Tags.GEBAEUDE));
        picDatabase.Add(CreatePicture("071", Tags.GESAMT, Tags.ZEIT_ABENDS, Tags.WOLKENANTEIL_MITTEL, Tags.WOLKEN_GEMISCHT, Tags.STRASSE, Tags.GEBAEUDE));
        picDatabase.Add(CreatePicture("072", Tags.GESAMT, Tags.ZEIT_ABENDS, Tags.WOLKENANTEIL_GERING, Tags.WOLKEN_HELL, Tags.STRASSE, Tags.GEBAEUDE));
        picDatabase.Add(CreatePicture("073", Tags.GESAMT, Tags.ZEIT_ABENDS, Tags.WOLKENANTEIL_GERING, Tags.WOLKEN_HELL, Tags.GEBAEUDE, Tags.STRASSE));
        picDatabase.Add(CreatePicture("074", Tags.GESAMT, Tags.ZEIT_ABENDS, Tags.WOLKENANTEIL_GERING, Tags.WOLKEN_HELL, Tags.BRUECKE, Tags.STRASSE, Tags.GEBAEUDE));
        picDatabase.Add(CreatePicture("075", Tags.GESAMT, Tags.ZEIT_ABENDS, Tags.WOLKENANTEIL_MITTEL, Tags.WOLKEN_HELL, Tags.STRASSE, Tags.GEBAEUDE));
        picDatabase.Add(CreatePicture("076", Tags.GESAMT, Tags.ZEIT_ABENDS, Tags.WOLKENANTEIL_GERING, Tags.WOLKEN_HELL));
        picDatabase.Add(CreatePicture("077", Tags.GESAMT, Tags.ZEIT_ABENDS, Tags.WOLKENANTEIL_GERING, Tags.WOLKEN_HELL, Tags.FELD));
        picDatabase.Add(CreatePicture("078", Tags.GESAMT, Tags.ZEIT_ABENDS, Tags.WOLKENANTEIL_GERING, Tags.WOLKEN_HELL, Tags.FELD));
        picDatabase.Add(CreatePicture("079", Tags.GESAMT, Tags.ZEIT_ABENDS, Tags.WOLKENANTEIL_GERING, Tags.WOLKEN_HELL, Tags.FELD));
        picDatabase.Add(CreatePicture("080", Tags.GESAMT, Tags.ZEIT_ABENDS, Tags.WOLKENANTEIL_GERING, Tags.WOLKEN_HELL, Tags.STRASSE, Tags.FELD));
        picDatabase.Add(CreatePicture("081", Tags.GESAMT, Tags.ZEIT_ABENDS, Tags.WOLKENANTEIL_GERING, Tags.WOLKEN_HELL, Tags.STRASSE, Tags.FELD));
        picDatabase.Add(CreatePicture("082", Tags.GESAMT, Tags.ZEIT_ABENDS, Tags.WOLKENANTEIL_GERING, Tags.WOLKEN_HELL, Tags.FELD));
        picDatabase.Add(CreatePicture("083", Tags.GESAMT, Tags.ZEIT_ABENDS, Tags.WOLKENANTEIL_MITTEL, Tags.WOLKEN_HELL, Tags.STRASSE));
        picDatabase.Add(CreatePicture("084", Tags.GESAMT, Tags.ZEIT_ABENDS, Tags.WOLKENANTEIL_GERING, Tags.WOLKEN_HELL, Tags.STRASSE));
        picDatabase.Add(CreatePicture("085", Tags.GESAMT, Tags.ZEIT_ABENDS, Tags.WOLKENANTEIL_GERING, Tags.WOLKEN_HELL, Tags.STRASSE));
        picDatabase.Add(CreatePicture("086", Tags.GESAMT, Tags.ZEIT_ABENDS, Tags.WOLKENANTEIL_GERING, Tags.WOLKEN_HELL, Tags.STRASSE, Tags.BRUECKE));
        picDatabase.Add(CreatePicture("087", Tags.GESAMT, Tags.ZEIT_ABENDS, Tags.WOLKENANTEIL_GERING, Tags.WOLKEN_HELL, Tags.STRASSE));
        picDatabase.Add(CreatePicture("088", Tags.GESAMT, Tags.ZEIT_ABENDS, Tags.WOLKENANTEIL_MITTEL, Tags.WOLKEN_HELL, Tags.STRASSE, Tags.GEBAEUDE));
        picDatabase.Add(CreatePicture("089", Tags.GESAMT, Tags.ZEIT_MORGENS, Tags.WOLKENANTEIL_MITTEL, Tags.WOLKEN_HELL, Tags.STRASSE, Tags.GEBAEUDE));
        picDatabase.Add(CreatePicture("090", Tags.GESAMT, Tags.ZEIT_MORGENS, Tags.WOLKENANTEIL_GERING, Tags.WOLKEN_HELL));
        picDatabase.Add(CreatePicture("091", Tags.GESAMT, Tags.ZEIT_MORGENS, Tags.WOLKENANTEIL_MITTEL, Tags.WOLKEN_HELL, Tags.STRASSE, Tags.GEBAEUDE));
        picDatabase.Add(CreatePicture("092", Tags.GESAMT, Tags.ZEIT_MORGENS, Tags.WOLKENANTEIL_GERING, Tags.WOLKEN_HELL, Tags.GEBAEUDE));
        picDatabase.Add(CreatePicture("093", Tags.GESAMT, Tags.ZEIT_MORGENS, Tags.WOLKENANTEIL_HOCH, Tags.WOLKEN_HELL, Tags.GEBAEUDE, Tags.STRASSE));
        picDatabase.Add(CreatePicture("094", Tags.GESAMT, Tags.ZEIT_MORGENS, Tags.WOLKENANTEIL_HOCH, Tags.WOLKEN_GEMISCHT, Tags.STRASSE));
        picDatabase.Add(CreatePicture("095", Tags.GESAMT, Tags.ZEIT_MORGENS, Tags.WOLKENANTEIL_HOCH, Tags.WOLKEN_GEMISCHT, Tags.STRASSE, Tags.FELD));
        picDatabase.Add(CreatePicture("096", Tags.GESAMT, Tags.ZEIT_MORGENS, Tags.WOLKENANTEIL_HOCH, Tags.WOLKEN_HELL, Tags.FELD));
        picDatabase.Add(CreatePicture("097", Tags.GESAMT, Tags.ZEIT_MORGENS, Tags.WOLKENANTEIL_HOCH, Tags.WOLKEN_GEMISCHT, Tags.FELD, Tags.STRASSE));
        picDatabase.Add(CreatePicture("098", Tags.GESAMT, Tags.ZEIT_MORGENS, Tags.WOLKENANTEIL_HOCH, Tags.WOLKEN_GEMISCHT, Tags.FELD, Tags.STRASSE));
        picDatabase.Add(CreatePicture("099", Tags.GESAMT, Tags.ZEIT_MORGENS, Tags.WOLKENANTEIL_HOCH, Tags.WOLKEN_GEMISCHT, Tags.FELD));
        picDatabase.Add(CreatePicture("100", Tags.GESAMT, Tags.ZEIT_MORGENS, Tags.WOLKENANTEIL_HOCH, Tags.WOLKEN_GEMISCHT, Tags.FELD));
        picDatabase.Add(CreatePicture("101", Tags.GESAMT, Tags.ZEIT_MORGENS, Tags.WOLKENANTEIL_HOCH, Tags.WOLKEN_GEMISCHT, Tags.FELD, Tags.STRASSE));
        picDatabase.Add(CreatePicture("102", Tags.GESAMT, Tags.ZEIT_MORGENS, Tags.WOLKENANTEIL_HOCH, Tags.WOLKEN_GEMISCHT, Tags.FELD));
        picDatabase.Add(CreatePicture("103", Tags.GESAMT, Tags.ZEIT_MORGENS, Tags.WOLKENANTEIL_HOCH, Tags.WOLKEN_GEMISCHT, Tags.FELD));

        //TODO add all pictures and their tags once collected!
    }

    private Picture CreatePicture(string name, params Tags[] tags)
    {
        Picture pic = new Picture();
        pic.name = name;
        foreach (Tags tag in tags)
        {
            pic.tags.Add(tag);
        }
        return pic;
    }

    public float[] CalculateStatistics(List<Result> resultsList)
    {
        //collects all precisions for each tag in the list
        float[] precisionArray = new float[Enum.GetNames(typeof(Tags)).Length];
        //counts how often a value was added
        int position = 0;
        int pos = 0;
        int[] countArray = new int[Enum.GetNames(typeof(Tags)).Length];
        try
        {
            foreach (Result res in resultsList)
            {
                //position in the picDatabase list depends on the name of the result:
                // 001 --> pos 0
                // 002 --> pos 1
                // quick conversion to int and subtracting 1 results in the correct position
                pos = Int16.Parse(res.name);
                position = pos - 1;
                //grab all of the pictures tags
                foreach (Tags tag in picDatabase[position].tags)
                {
                    //add the precision to the corresponding position of the precisionArray
                    precisionArray[(int) tag] += res.precision;
                    countArray[(int) tag]++;
                }
            }
        }
        catch (Exception e)
        {
            print(e.Message);
        }
        //average value calculation
        float[] resultsArray = new float[Enum.GetNames(typeof(Tags)).Length];
        for (int i = 0; i < resultsArray.Length; i++)
        {
            resultsArray[i] = precisionArray[i] / (float) countArray[i];
        }
        //return average values, positions correspond to the order of the tags in the enum!
        return resultsArray;
    }
}

public class Picture
{
    public string name;
    public List<PictureClassificationDatabase.Tags> tags = new List<PictureClassificationDatabase.Tags>();
}

[System.Serializable]
public class ResultsOverview
{
    public float[] resultsArray;
}