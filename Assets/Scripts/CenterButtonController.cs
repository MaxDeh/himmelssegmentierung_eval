using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.PlayerLoop;
using UnityEngine.UI;
using static SegmentationProcessManager;

[RequireComponent(typeof(Button))]
public class CenterButtonController : MonoBehaviour, IPointerClickHandler
{
    [SerializeField] SegmentationProcessManager segManager;

    private void Update()
    {
        if (Input.GetKey(KeyCode.Space))
        {
            OnPointerClick(new PointerEventData(EventSystem.current));
        }
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        SegState segState = segManager.GetSegState();
        switch (segState)
        {
            case SegState.SkyAreaSelection:
                break;
            case SegState.SkyConfirmAreaSelection:
                segManager.ProgressSegState();
                break;
            case SegState.SkyThresholdAdjustment:
                segManager.ProgressSegState();
                break;
            case SegState.CloudAreaSelection:
                break;
            case SegState.CloudConfirmAreaSelection:
                segManager.ProgressSegState();
                break;
            case SegState.CloudThresholdAdjustment:
                segManager.ProgressSegState();
                break;
        }
    }
}
