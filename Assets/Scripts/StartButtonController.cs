using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public class StartButtonController : MonoBehaviour, IPointerClickHandler
{
    [SerializeField] private GameObject mainUI;
    [SerializeField] private SegmentationProcessManager segManager;

    public void OnPointerClick(PointerEventData eventData)
    {
        mainUI.SetActive(true);
        segManager.EnterSkyAreaSelectionMode();
        gameObject.SetActive(false);
    }
}
