using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Result
{
    public string name;
    public int[] ResultBools;
    public float precision;
    public Result(string name, int[] results)
    {
        this.name = name;
        ResultBools = results;
    }
}
