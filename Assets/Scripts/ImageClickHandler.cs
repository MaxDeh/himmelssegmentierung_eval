using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
public class ImageClickHandler : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    [SerializeField] SegmentationProcessManager segManager;

    public void OnPointerDown(PointerEventData eventData)
    {
        if (segManager.GetSegState() == SegmentationProcessManager.SegState.SkyAreaSelection || 
            segManager.GetSegState() == SegmentationProcessManager.SegState.CloudAreaSelection) segManager.OnFingerDown(eventData);
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        if (segManager.GetSegState() == SegmentationProcessManager.SegState.SkyAreaSelection ||
            segManager.GetSegState() == SegmentationProcessManager.SegState.CloudAreaSelection) segManager.OnFingerUp(eventData);
    }
}
