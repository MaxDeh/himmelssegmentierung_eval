using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PrecisionCalculator : MonoBehaviour
{
    public float CalculatePrecision(Result result)
    {
        string groundtruthFilepath = "Images/GroundTruthImages/" + result.name;
        Sprite groundTruthSprite = Resources.Load<Sprite>(groundtruthFilepath);
        Color[] gtColorArray = groundTruthSprite.texture.GetPixels();
        int hitCounter = 0;
        for (int i = 0; i < gtColorArray.Length; i++)
        {
            if (gtColorArray[i] == Color.white && result.ResultBools[i] == 1) hitCounter++;
            if (gtColorArray[i] == Color.black && result.ResultBools[i] == 0) hitCounter++;
        }
        float precision = (float)hitCounter / gtColorArray.Length;
        return precision;
        //string filename = String.Format("{0:000}", progress);
        //string path = Application.persistentDataPath + "/Results/" + filename + ".json";
        //String jsonString = File.ReadAllText(path);
        //Result readResult = JsonUtility.FromJson<Result>(jsonString);
    }
}
